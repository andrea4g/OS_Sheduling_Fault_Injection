/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/
#include "stm32f3xx.h"
#include "stm32f3_discovery.h"
#include "stm32f3_discovery_accelerometer.h" //accelerometer
#include "stm32f3xx_hal_tim.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
GPIO_InitTypeDef GPIO_InitStructure;

/* Private function prototypes -----------------------------------------------*/
void vTask1( void *pvParameters );
void vTask2( void *pvParameters );
void AccelerometerTask ( void *pvParameters );

/* Private functions ---------------------------------------------------------*/
uint32_t Redefined_SysTick_Config(uint32_t Tick_to_be_counted){
  if ((Tick_to_be_counted - 1UL) > 0xFFFFFFUL) /*if bigger than 2^24 error*/
    return (1UL);   /* Reload value impossible */

  /* Load the SysTick Counter Value */
  SysTick->VAL   = 0UL;

  /* set reload register */
  SysTick->LOAD  = (uint32_t)(Tick_to_be_counted - 1UL);

  /* set Priority for Systick Interrupt (lower value means higher priority: 0 is max priority)*/
  NVIC_SetPriority (SysTick_IRQn, 4);

  /* set control register */
  uint32_t MASK_SysTick_CLK_SOURCE = 0 ;     /*0: AHB/8
                       1: Processor clock (AHB)  */
  uint32_t MASK_SysTick_CTRL_TICKINT = 1;    /*SysTick exception request enable
  0: Counting down to zero does not assert the SysTick exception request
    1: Counting down to zero to asserts the SysTick exception request.
  Note: Software can use COUNTFLAG to determine if SysTick has ever counted to zero. */
  uint32_t MASK_SysTick_CTRL_ENABLE = 1 ;    /*Counter enable
  Enables the counter. When ENABLE is set to 1, the counter loads the RELOAD value from the
  LOAD register and then counts down. On reaching 0, it sets the COUNTFLAG to 1 and
  optionally asserts the SysTick depending on the value of TICKINT. It then loads the RELOAD
  value again, and begins counting.
  0: Counter disabled
  1: Counter enabled*/
  SysTick->CTRL  = MASK_SysTick_CLK_SOURCE  |  MASK_SysTick_CTRL_TICKINT  |  MASK_SysTick_CTRL_ENABLE;
  return (0UL);  /* Function successful */
}

int main(void)
{
  /*!< At this stage the microcontroller clock setting is already configured,
       this is done through SystemInit() function which is called from startup
       file (startup_stm32f303x.s) before to branch to application main.
       To reconfigure the default setting of SystemInit() function, refer to
       system_stm32f3xx.c file  */


  /*------Output Compare----------- */

  TIM_HandleTypeDef htim3;
  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;

  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 0;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 65535;
  htim3.Init.ClockDivision = 00; //00: tDTS = tCK_INT

  if (HAL_TIM_Base_Init(&htim3) != HAL_OK){
      Error_Handler();
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;

  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)  {
      Error_Handler();
  }
  if (HAL_TIM_OC_Init(&htim3) != HAL_OK)  {
       Error_Handler();
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;

  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)  {
      Error_Handler();
  }

  sConfigOC.OCMode = TIM_OCMODE_TIMING;
  sConfigOC.Pulse = 0x1000;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_OC_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)  {
      Error_Handler();
  }
  sConfigOC.Pulse = 0x10000;
  if (HAL_TIM_OC_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)  {
      Error_Handler();
  }

  /* Initialize Leds mounted on STM32F3-discovery */
  BSP_LED_Init(LED_BLUE);
  BSP_LED_Init(LED_RED);
  BSP_LED_Init(LED_ORANGE);
  BSP_LED_Init(LED_GREEN);
  BSP_LED_Init(LED_BLUE_2);
  BSP_LED_Init(LED_RED_2);
  BSP_LED_Init(LED_ORANGE_2);
  BSP_LED_Init(LED_GREEN_2);

  /* Initialize Accelerometer and Gyroscope */
  BSP_ACCELERO_Init();
  BSP_GYRO_Init();

  /* Resetting Accelerometer and Gyroscope (is it useful?) */
  BSP_GYRO_Reset();
  BSP_ACCELERO_Reset();

  /* Create one of the two tasks. Note that a real application should check
  the return value of the xTaskCreate() call to ensure the task was created
  successfully. */
  xTaskCreate( vTask1, /* Pointer to the function that implements the task. */
  "Task 1",/* Text name for the task. This is to facilitate
  debugging only. */
  1000, /* Stack depth - small microcontrollers will use much
  less stack than this. */
  NULL, /* This example does not use the task parameter. */
  1, /* This task will run at priority 1. */
  NULL ); /* This example does not use the task handle. */

  /* Create the other task in exactly the same way and at the same priority. */
  xTaskCreate( vTask2, "Task 2", 1000, NULL, 1, NULL );

  /* Accelerometer Task */
  xTaskCreate( AccelerometerTask, "Accelerometer", 1000, NULL, 1, NULL );
  /* Gyroscope Task */
  xTaskCreate( GyroscopeTask, "Gyroscope", 1000, NULL, 1, NULL );

  /* Start the scheduler so the tasks start executing. */
  vTaskStartScheduler();

  /* If all is well then main() will never reach here as the scheduler will
  now be running the tasks. If main() does reach here then it is likely that
  there was insufficient heap memory available for the idle task to be created.
  Chapter 2 provides more information on heap memory management. */
  for( ;; );
}

void vTask1( void *pvParameters ){
  while (1) {
    /* Toggle LED3 */
    BSP_LED_Toggle(LED3);
    /* Toggle LED7 */
    BSP_LED_Toggle(LED7);
    /* Toggle LED6 */
    BSP_LED_Toggle(LED6);
    /* Toggle LED10 */
    BSP_LED_Toggle(LED10);

    int i = 0;
    for( i = 0; i < 5000000; i++ );
  }
}

void vTask2( void *pvParameters ){
  while (1){
    int i = 0;
    for( i = 0; i < 5000000; i++ );

    /* Toggle LED4 */
    BSP_LED_Toggle(LED4);
    /* Toggle LED5 */
    BSP_LED_Toggle(LED5);
    /* Toggle LED9 */
    BSP_LED_Toggle(LED9);
    /* Toggle LED8 */
    BSP_LED_Toggle(LED8);
  }
}

void AccelerometerTask ( void *pvParameters ){
  int16_t pDataXYZ;
  BSP_ACCELERO_GetXYZ(*pDataXYZ);
}

void GyroscopeTask ( void *pvParameters ){
  float pfData;
  BSP_GYRO_GetXYZ(float* pfData)
}

// *pvPortMalloc
// vPortFree

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

   /* if (SysTick_Config(SystemCoreClock / 1000))
    {
      // Capture error
      while (1);
    }



  int i=0;
  while(1)
      {
    BSP_LED_Toggle(LED_GREEN);
        for( i = 0; i < 50000; i++ );
    BSP_LED_Toggle(LED_BLUE);
        for( i = 0; i < 50000; i++ );
    BSP_LED_Toggle(LED_RED);
        for( i = 0; i < 50000; i++ );
    BSP_LED_Toggle(LED_ORANGE);
        for( i = 0; i < 50000; i++ );
    BSP_LED_Toggle(LED_GREEN_2);
        for( i = 0; i < 50000; i++ );
    BSP_LED_Toggle(LED_BLUE_2);
        for( i = 0; i < 50000; i++ );
    BSP_LED_Toggle(LED_RED_2);
        for( i = 0; i < 50000; i++ );
    BSP_LED_Toggle(LED_ORANGE_2);
        for( i = 0; i < 50000; i++ );

      }
}
*/
