################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
S_SRCS += \
../startup/startup_stm32f303xc.s 

OBJS += \
./startup/startup_stm32f303xc.o 


# Each subdirectory must supply rules for building sources it contributes
startup/%.o: ../startup/%.s
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Assembler'
	@echo %cd%
	arm-none-eabi-as -mcpu=cortex-m4 -mthumb -mfloat-abi=soft -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/inc" -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/CMSIS/core" -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/CMSIS/device" -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/HAL_Driver/Inc/Legacy" -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/HAL_Driver/Inc" -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/Utilities/Components/Common" -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/Utilities/Components/l3gd20" -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/Utilities/Components/lsm303dlhc" -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/Utilities/STM32F3-Discovery" -g -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


