################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Utilities/STM32F3-Discovery/stm32f3_discovery.c \
../Utilities/STM32F3-Discovery/stm32f3_discovery_accelerometer.c \
../Utilities/STM32F3-Discovery/stm32f3_discovery_gyroscope.c 

OBJS += \
./Utilities/STM32F3-Discovery/stm32f3_discovery.o \
./Utilities/STM32F3-Discovery/stm32f3_discovery_accelerometer.o \
./Utilities/STM32F3-Discovery/stm32f3_discovery_gyroscope.o 

C_DEPS += \
./Utilities/STM32F3-Discovery/stm32f3_discovery.d \
./Utilities/STM32F3-Discovery/stm32f3_discovery_accelerometer.d \
./Utilities/STM32F3-Discovery/stm32f3_discovery_gyroscope.d 


# Each subdirectory must supply rules for building sources it contributes
Utilities/STM32F3-Discovery/%.o: ../Utilities/STM32F3-Discovery/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=soft -DSTM32F30 -DSTM32F3DISCOVERY -DSTM32F3 -DSTM32F303VCTx -DSTM32 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F303xC -DUSE_RTOS_SYSTICK -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/inc" -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/CMSIS/core" -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/CMSIS/device" -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/HAL_Driver/Inc/Legacy" -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/HAL_Driver/Inc" -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/Utilities/Components/Common" -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/Utilities/Components/l3gd20" -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/Utilities/Components/lsm303dlhc" -I"C:/Users/Andrea/workspace/OS_Sheduling_Fault_Injection/CLK_and_ISR/Utilities/STM32F3-Discovery" -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


